﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ParserLibrary;

namespace GraphLibrary
{
    public class GraphSharp : IGraphLib
    {
        private List<Node> nodes;
        private List<Edge> edges;

        public GraphSharp(Graph graph)
        {
            nodes = graph.Nodes;
            edges = graph.Edges;
        }

        public void CreateFile(string path)
        {

            using (
                XmlWriter writer = XmlWriter.Create(path,
                    new XmlWriterSettings {Encoding = Encoding.UTF8, Indent = true}))
            {
                writer.WriteStartDocument();

                writer.WriteStartElement("graphml", "http://graphml.graphdrawing.org/xmlns");
                    //<graphml xmlns="http://graphml.graphdrawing.org/xmlns">

                writer.WriteStartElement("graph"); //<graph>

                writer.WriteAttributeString("id", "G");
                writer.WriteAttributeString("parse.nodes", "8");
                writer.WriteAttributeString("parse.edges", "7");
                writer.WriteAttributeString("parse.order", "nodesfirst");
                writer.WriteAttributeString("parse.nodeids", "free");
                writer.WriteAttributeString("parse.edgeids", "free");

                foreach (var node in nodes)
                {
                    writer.WriteStartElement("node");
                    writer.WriteAttributeString("id", node.ID);
                    writer.WriteEndElement();
                }

                foreach (var edge in edges)
                {
                    writer.WriteStartElement("edge");

                    writer.WriteAttributeString("id", edge.ID);
                    writer.WriteAttributeString("source", edge.Source);
                    writer.WriteAttributeString("target", edge.Target);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement(); //</graphml>

                writer.WriteEndElement(); //</graph>

                writer.WriteEndDocument();
            }
        }
    }
}
