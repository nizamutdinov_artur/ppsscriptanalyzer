﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ParserLibrary;

namespace GraphLibrary
{
    public class GEXF : IGraphLib
    {            
        private List<Node> nodes;
        private List<Edge> edges;

        public GEXF(Graph graph)
        {
            nodes = graph.Nodes;
            edges = graph.Edges;
        }

        public void CreateFile(string path)
        {
            using (
                XmlWriter writer = XmlWriter.Create(path,
                    new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true }))
            {
                writer.WriteStartDocument();

                writer.WriteStartElement("gexf", "http://www.gexf.net/1.2draft");
                //<graphml xmlns="http://graphml.graphdrawing.org/xmlns">

                writer.WriteStartElement("graph"); //<graph>

                writer.WriteAttributeString("mode", "dynamic");
                writer.WriteAttributeString("defaultedgetype", "directed");

                writer.WriteStartElement("nodes"); //<nodes>

                foreach (var node in nodes)
                {
                    writer.WriteStartElement("node");

                    writer.WriteAttributeString("id", node.ID);
                    writer.WriteAttributeString("label", node.Label);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement(); //</nodes>

                writer.WriteStartElement("edges"); //<egdes>


                foreach (var edge in edges)
                {
                    writer.WriteStartElement("edge");

                    writer.WriteAttributeString("id", edge.ID);
                    writer.WriteAttributeString("source", edge.Source);
                    writer.WriteAttributeString("target", edge.Target);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement(); //</edges>


                writer.WriteEndElement(); //</graphml>

                writer.WriteEndElement(); //</graph>

                writer.WriteEndDocument();
            }
        }
    }
}
