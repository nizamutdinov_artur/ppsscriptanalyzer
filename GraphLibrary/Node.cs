﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLibrary
{

    public class Node
    {
        public string ID { get; set; }
        public string Label { get; set; }

        public Node(string id, string label)
        {
            ID = id;
            Label = label;
        }
    }

}
