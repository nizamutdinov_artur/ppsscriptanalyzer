﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLibrary
{
    public class Edge
    {
        public string ID { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }

        public Edge(string id, string source, string target)
        {
            ID = id;
            Source = source;
            Target = target;
        }
    }
}
