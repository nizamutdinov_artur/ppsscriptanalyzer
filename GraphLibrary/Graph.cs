﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParserLibrary;

namespace GraphLibrary
{
    public class Graph
    {
        public List<Node> Nodes { get; set; }
        public List<Edge> Edges { get; set; }

        public Graph(State[] states)
        {
            Nodes = new List<Node>();
            Edges = new List<Edge>();

            Initialize(states);
        }

        private void Initialize(State[] states)
        {
            InitNodes(states);
            InitEdges(states);
        }

        private void InitNodes(State[] states)
        {
            for (int i = 0; i < states.Length; i++)
                Nodes.Add(new Node(i.ToString(), states[i].ToString()));

        }

        private void InitEdges(State[] states)
        {
            var edgeID = 0;

            for (int i = 0; i < states.Length; i++)
            {
                foreach (var linkedState in states[i].LinkedStates)
                {
                    for (int j = 0; j < states.Length; j++)
                    {
                        if (linkedState == states[j])
                        {
                            Edges.Add(new Edge(
                                edgeID.ToString(),
                                i.ToString(),
                                j.ToString()
                                ));

                            edgeID++;
                        }
                    }
                }


            }

        }
    }
}
