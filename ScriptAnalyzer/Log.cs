﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParserLibrary;

namespace ScriptAnalyzer
{
    class Log
    {
        public static void LogToFile(string text, string fileName)
        {
            if (!Directory.Exists(GetLogsDirectory()))
                Directory.CreateDirectory(GetLogsDirectory());

            if (fileName == String.Empty || text == null)
           return;

            File.AppendAllText(GetLogsPath(fileName), text);
        }

        public static void LogLinesToFile(string[] lines, string fileName)
        {
            if (!Directory.Exists(GetLogsDirectory()))
                Directory.CreateDirectory(GetLogsDirectory());

            if (lines == null || fileName == String.Empty)
               return; 

            foreach (var line in lines)
            File.AppendAllText(GetLogsPath(fileName), line + "\r\n");
        }

        private static string GetLogsPath(string fileName)
        {
            return GetLogsDirectory()+"//" + fileName;
        }

        private static string GetLogsDirectory()
        {
            return Directory.GetCurrentDirectory() + "//Logs";
        }

        public static string GetDependences(State state, string fileName)
        {
            string output = $"Состояние: {state.ScriptName}->{state.StateName} имеет ссылки на: \r\n\t";

            foreach (var child in state.LinkedStates)
                output = output + $"Состояние: {child.ScriptName}->{child.StateName}\r\n\t";

            output += "\r\n\r\n";

            return output;
        }

        public static void LogDependences(State[] states, string fileName)
        {
            LogToFile(states.Aggregate("", (current, state) => current + GetDependences(state, fileName)), fileName);
        }
    }
}
