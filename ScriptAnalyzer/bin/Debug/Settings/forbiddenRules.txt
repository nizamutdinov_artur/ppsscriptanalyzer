# Объекты подходящие под данные выражения не будут включены в результат поиска.
# Правила могут содержать несколько регулярок в одной строке.
#
Attributes[\w\W]+?{[\w\W]+?PropertyKey
Application.Errors.CheckStatesAndSetPropertyValue
Application.Properties.SetValueSwitchCaseValue
