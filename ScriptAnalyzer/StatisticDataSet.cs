﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriptAnalyzer
{
   public class StatisticDataSet
    {
       public string Name { get; set; }
       public string Value { get; set; }

       public StatisticDataSet(string name, string value)
       {
           Name = name;
           Value = value;
       }
    }
}
