﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ScriptAnalyzer.Converters
{
    class NullColorIndicatorDataConverter    : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value != null) ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Crimson);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
