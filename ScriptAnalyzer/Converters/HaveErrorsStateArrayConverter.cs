﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using ParserLibrary;

namespace ScriptAnalyzer.Converters
{
    class HaveErrorsStateArrayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var states = value as List<State>;

            if (states == null)
                return "";

            var emptyLinksCount = states.Count(linkedState => linkedState.Data == null);

            return (emptyLinksCount > 0)
                ? $"Ошибок: {emptyLinksCount}"
                : "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
