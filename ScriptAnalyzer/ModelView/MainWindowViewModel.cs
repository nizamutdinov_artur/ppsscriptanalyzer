﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using GraphLibrary;
using ParserLibrary;

namespace ScriptAnalyzer.ModelView
{
    class MainWindowViewModel : DependencyObject
    {
        private StateParser stateParser;

        public Graph Graph { get; set; }

        public State[] States
        {
            get { return StateCollectionView.SourceCollection.Cast<State>().ToArray(); }
            set { StateCollectionView = CollectionViewSource.GetDefaultView(value); }
        }

        public ICollectionView StateCollectionView
        {
            get { return (ICollectionView) GetValue(StateItemsProperty); }
            set { SetValue(StateItemsProperty, value); }
        }

        public ICommand ScanButtonCommand { get; set; }
        public ICommand ExportButtonCommand { get; set; }
        public ICommand ExportLogButtonCommand { get; set; }

        /// <summary>
        /// Конструктор ViewModel
        /// </summary>
        public MainWindowViewModel()
        {
            stateParser = new StateParser("MGTIdle", "main");

            ScanButtonCommand = new RelayCommand(args => ParseStates());
            ExportButtonCommand = new RelayCommand(args => ExportGraph());
            ExportLogButtonCommand = new RelayCommand(args => ExportLog());

            StateCollectionView = new CollectionView(new State[] {});   
            StatisticData = new List<StatisticDataSet>();
            Graph = new Graph(new State[] {});

            StateCollectionView.Filter = FilterState;

        }

        #region DependencyPropertyBackingStore

        // Using a DependencyProperty as the backing store for States.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StateItemsProperty =
            DependencyProperty.Register("StateCollectionView", typeof (ICollectionView), typeof (MainWindowViewModel),
                new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for StatisticData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatisticDataProperty =
            DependencyProperty.Register("StatisticData", typeof (List<StatisticDataSet>), typeof (MainWindowViewModel),
                new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ParserIsReady.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParserIsReadyProperty =
            DependencyProperty.Register("ParserIsReady", typeof (bool), typeof (MainWindowViewModel),
                new PropertyMetadata(true));

        // Using a DependencyProperty as the backing store for ComboBoxSelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ComboBoxSelectedItemProperty =
            DependencyProperty.Register("ComboBoxSelectedItem", typeof (int), typeof (MainWindowViewModel),
                new PropertyMetadata(0, ComboBoxSelectionChanged));

        // Using a DependencyProperty as the backing store for ComboBoxSelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExportComboBoxSelectedItemProperty =
            DependencyProperty.Register("ExportComboBoxSelectedItem", typeof (int), typeof (MainWindowViewModel),
                new PropertyMetadata(0));

        // Using a DependencyProperty as the backing store for NameTextBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NameTextBoxProperty =
            DependencyProperty.Register("NameTextBox", typeof (string), typeof (MainWindowViewModel),
                new PropertyMetadata(""));

        // Using a DependencyProperty as the backing store for ComboBoxSelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExportLogComboBoxSelectedItemProperty =
            DependencyProperty.Register("ExportLogComboBoxSelectedItem", typeof (int), typeof (MainWindowViewModel),
                new PropertyMetadata(0));

        // Using a DependencyProperty as the backing store for NameTextBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LogNameTextBoxProperty =
            DependencyProperty.Register("LogNameTextBox", typeof (string), typeof (MainWindowViewModel),
                new PropertyMetadata(""));

        // Using a DependencyProperty as the backing store for ScriptNameTextBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScriptNameTextBoxProperty =
            DependencyProperty.Register("ScriptNameTextBox", typeof(string), typeof(MainWindowViewModel), new PropertyMetadata("", StateTextBoxChangedCallback));
        
        // Using a DependencyProperty as the backing store for StateNameTextBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StateNameTextBoxProperty =
            DependencyProperty.Register("StateNameTextBox", typeof(string), typeof(MainWindowViewModel), new PropertyMetadata("", StateTextBoxChangedCallback));
         
        #endregion

        #region UIProperties

        public bool ParserIsReady
        {
            get { return (bool) GetValue(ParserIsReadyProperty); }
            set { SetValue(ParserIsReadyProperty, value); }
        }

        public List<StatisticDataSet> StatisticData
        {
            get { return (List<StatisticDataSet>) GetValue(StatisticDataProperty); }
            set { SetValue(StatisticDataProperty, value); }
        }

        public int ComboBoxSelectedItem
        {
            get { return (int) GetValue(ComboBoxSelectedItemProperty); }
            set { SetValue(ComboBoxSelectedItemProperty, value); }
        }

        public int ExportComboBoxSelectedItem
        {
            get { return (int) GetValue(ExportComboBoxSelectedItemProperty); }
            set { SetValue(ExportComboBoxSelectedItemProperty, value); }
        }

        public string NameTextBox
        {
            get { return (string) GetValue(NameTextBoxProperty); }
            set { SetValue(NameTextBoxProperty, value); }
        }

        public int ExportLogComboBoxSelectedItem
        {
            get { return (int) GetValue(ExportLogComboBoxSelectedItemProperty); }
            set { SetValue(ExportLogComboBoxSelectedItemProperty, value); }
        }

        public string LogNameTextBox
        {
            get { return (string) GetValue(LogNameTextBoxProperty); }
            set { SetValue(LogNameTextBoxProperty, value); }
        }
         
        public string ScriptNameTextBox
        {
            get { return (string)GetValue(ScriptNameTextBoxProperty); }
            set { SetValue(ScriptNameTextBoxProperty, value); }
        }
                     
        public string StateNameTextBox
        {
            get { return (string)GetValue(StateNameTextBoxProperty); }
            set { SetValue(StateNameTextBoxProperty, value); }
        }
        
        #endregion


        private static void ComboBoxSelectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs dArgs)
        {
            var classInstance = d as MainWindowViewModel;
            if (classInstance == null) return;

            classInstance.StateCollectionView.Filter = null;
            classInstance.StateCollectionView.Filter = classInstance.FilterState;
        }

        private static void StateTextBoxChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs dArgs)
        {
            var classInstance = d as MainWindowViewModel;
            if (classInstance == null) return;

            classInstance.stateParser.SetRootState(classInstance.ScriptNameTextBox, classInstance.StateNameTextBox);
        }

         
        private bool FilterState(object obj)
        {
            var state = obj as State;

            if (state.Equals(null) || state.Data == null) return false;

            return (ComboBoxSelectedItem == 0)
                ? true
                : (ComboBoxSelectedItem == 1)
                    ? !state.LinkedStates.Any(s => s.Data == null)
                    : state.LinkedStates.Any(s => s.Data == null);
        }

        private void ExportGraph()
        {
            IGraphLib graphLib;

            if (ExportComboBoxSelectedItem == 0)
                graphLib = new GEXF(Graph);
            else
                graphLib = new GraphSharp(Graph);


            graphLib.CreateFile(NameTextBox);
        }

        private void ExportLog()
        {
            switch (ExportLogComboBoxSelectedItem)
            {
                case 0:
                    Log.LogLinesToFile(
                        stateParser.Messages.GetMessages(MessageType.Error), LogNameTextBox);
                    break;
                case 1:
                    Log.LogDependences(States, LogNameTextBox);
                    break;
                case 2:
                    Log.LogLinesToFile(
                        stateParser.Messages.GetMessages(MessageType.Open), LogNameTextBox);
                    break;
                case 3:
                    Log.LogLinesToFile(
                        stateParser.GetUnusedStates(States).Select(unUsedState => unUsedState.ToString()).ToArray(),
                        LogNameTextBox);
                    break;
            }
        }

        private async void ParseStates()
        {

            ParserIsReady = false;

            stateParser = null;

            stateParser = new StateParser(ScriptNameTextBox,StateNameTextBox);
            stateParser.Messages.Clear();

            var states = await Task<State[]>.Factory.StartNew(stateParser.GenerateStateTree);

            StateCollectionView = CollectionViewSource.GetDefaultView(states);
            StateCollectionView.Filter = FilterState;

            UpdateStaticticData();

            ParserIsReady = true;
        }

        private void UpdateStaticticData()
        {
            Graph = null;
            Graph = new Graph(States);

            StatisticData = null;
            StatisticData = new List<StatisticDataSet>();

            StatisticData.Add(new StatisticDataSet("Пустых ссылок",
                stateParser.Messages.GetMessages(MessageType.Error).Length.ToString()));

            StatisticData.Add(new StatisticDataSet("Вершин графа",
                Graph.Nodes.Count.ToString()));

            StatisticData.Add(new StatisticDataSet("Ребер графа",
                Graph.Edges.Count.ToString()));
        }

    }
}
