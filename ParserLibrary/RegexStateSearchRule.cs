﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ParserLibrary
{

   public class RegexStateSearchRule
   {
       private string[] patterns;

       public RegexStateSearchRule(string[] patterns)
       {
           this.patterns = patterns;
       }

       public string[] Patterns
       {
           get { return patterns; }
           set { patterns = value; }
       }

        /// <summary>
        /// Получает дочерние состояния из указанного строки JObject.
        /// </summary>
        /// <param name="jObject">Строка JSON.</param>
        /// <param name="scriptName">Имя текущего сценария.</param>
        /// <param name="fileReader">The file reader.</param>
        /// <returns>
        /// Дочерние состояния.
        /// </returns>
        public State[] GetStates(string jObject, string scriptName, FileReader fileReader)
       {
           var content = jObject;
           var result = new List<State>();

           for (int i = 0; i < patterns.Length; i++)
           {
               if (i == patterns.Length - 1)
               {
                   Match match = Regex.Match(content, patterns[i]);

                   while (match.Success)
                   {
                       result.Add(new State(scriptName, match.Groups[1].Value, fileReader));
                       match = match.NextMatch();
                   }

                   break;
               }

               content = Regex.Match(content, patterns[i]).Groups[1].Value;
           }

           return result.ToArray();

       }


   }
}
