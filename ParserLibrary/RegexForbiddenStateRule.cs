﻿using System.Text.RegularExpressions;

namespace ParserLibrary
{
    public class RegexForbiddenStateRule
    {
        private string[] patterns;

        public string[] Patterns
        {
            get { return patterns; }
            set { patterns = value; }
        }

        public RegexForbiddenStateRule(string[] patterns)
        {
            this.patterns = patterns;
        }

        public bool IsClear(string JSONObject)
        {
            var content = JSONObject;

            for (int i = 0; i < patterns.Length; i++)
            {

                if (i == patterns.Length - 1)
                {
                    if (Regex.Match(content, patterns[i]).Success)
                        return false;

                    break;
                }

                content = Regex.Match(content, patterns[i]).Groups[1].Value;
            }

            return true;

        }


    }
}