﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ParserLibrary
{

    /// <summary>
    /// TODO:                                                                               
    /// - При выводе лога указать состояние, которое ссылается на несуществующее состояние.
    /// - Remove duplicated linked nodes in stetes array.
    /// </summary>
    public class StateParser
    {
        private State rootState;      

        private readonly State[] scriptMainStates;

        private readonly RegexStateSearchRule[] searchRules;
        private readonly RegexForbiddenStateRule[] forbiddenRules;  

        public FileReader FileReader { get; set; }

        public Messages Messages { get { return Messages.GetInstance();} }
                                                
        /// <summary>
        /// Initializes a new instance of the <see cref="StateParser" /> class.
        /// </summary>
        /// <param name="rootScript">The root script.</param>
        /// <param name="rootState">State of the root.</param>
        public StateParser(string rootScript, string rootState)
        {               
            FileReader = new FileReader(Directory.GetCurrentDirectory() + "\\data");
            scriptMainStates = FileReader.GetScripts();

            this.rootState = new State(rootScript, rootState, FileReader);

            searchRules = FileReader.LoadSearchRules("Settings/searchRules.txt");
            forbiddenRules = FileReader.LoadForbiddenRules("Settings/forbiddenRules.txt");   
        }

        /// <summary>
        /// Root state setter
        /// </summary>
        /// <param name="scriptName">Name of the script.</param>
        /// <param name="stateName">Name of the state.</param>
        public void SetRootState(string scriptName, string stateName)
        {
            var state = new State(scriptName, stateName, FileReader);
            if (state.Data == null) { return; }

            rootState = state;
        }

        /// <summary>
        /// Gets all states.
        /// </summary>
        /// <param name="withData">if set to <c>true</c> [with data].</param>
        /// <returns></returns>
        public State[] GetAllStates(bool withData)
        {
            return FileReader.GetAllStates(withData);
        }

        public State[] GetUnusedStates(State[] states)
        {
            List<State> result = new List<State>();

            var allStates = GetAllStates(false);

            foreach (var allStateInstance in allStates)
            {
                bool have = false;

                foreach (var state in states)
                    if (state == allStateInstance)
                        have = true;

                if (!have)
                       result.Add(allStateInstance);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Собирает состояния с текущей строки JSON.
        /// </summary>
        /// <param name="JSON">Строка с JSON.</param>
        /// <param name="scriptName">Название сценария родительского состояния.</param>
        /// <returns>Массив найденных состояний.</returns>
        public State[] ParseStates(string JSON, string scriptName)
        {
            var result = new List<State>();

            foreach (var searchRule in searchRules)
            {
                var states = searchRule.GetStates(JSON, scriptName, FileReader);

                for (int i = 0; i < states.Length; i++)
                {
                    var state = states[i];

                    // Nothin to do 
                    if (state.StateName == "stay") continue;

                    SetScriptNameConstraints(ref state);

                    // Обновить данные JSON
                    // Нужно для обновления, после изменения имени состояния 
                    state.UpdateData(FileReader);

                    result.Add(state);
                }
            }

            return result.ToArray();
        }
        /// <summary>
        /// Подходит ли тип для обработки ?
        /// </summary>
        /// <returns>Bool.</returns>
        public bool ObjectIsClear(string JSON)
        {
            return forbiddenRules.All(rule => rule.IsClear(JSON));
        }

        /// <summary>
        /// Проверяет ссылается ли состояние на внешний сценарий
        /// Если ссылается, то состояние приравнивается начальному состоянию внешнего сценария.
        /// </summary>
        /// <param name="state">Состояние для проверки.</param>
        private void SetScriptNameConstraints(ref State state)
        {
            foreach (var script in scriptMainStates)
            {
                if (script.ScriptName != state.StateName) continue;
                                                                                                   
                state.StateName = script.StateName;
                state.ScriptName = script.ScriptName;
            }

        }

        /// <summary>
        /// Возвращает массив типа State содержащий все возможные переходы по состояниям.
        /// </summary>
        /// <returns>Все возможные переходы по состояниям.</returns>
        public State[] GenerateStateTree()
        {
            var result = new List<State>();
            var stack = new Stack<State>();
            State node;

            // Добавляет первоначальное состояние в стэк
            stack.Push(rootState);

            while (stack.Count > 0)
            {
                node = stack.Pop(); // Берем состояние из стека
                node.GetChildNodes(this); // Получаем его детей

                result.Add(node); // Добавляем состояние в найденные состояния

                // Для каждого дочернего состояния текущего состояния проверить, нужно ли его добавить в очередь на обработку.
                // Если нужно, то это состояние будет использовано для поиска дочерних.
                foreach (State linkedState in node.LinkedStates)
                {
                    if (!IsDuplicate(result, linkedState))
                        stack.Push(linkedState);
                }
            }

            // Return only unique states
            return result.Distinct().ToArray();
        }

        /// <summary>
        /// Проверяет есть ли указанное состояние в найденных состояниях.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="states">The states.</param>
        /// <returns>Bool.</returns>
        private bool IsDuplicate(IEnumerable<State> states, State state)
        {
            foreach (var _state in states)
                if (state == _state)
                    return true;

            return false;
        }

    }
}