﻿using System.Collections.Generic;

namespace ParserLibrary
{
    /// <summary>
    /// Состояние.
    /// </summary>
    public class State
    {
               
        public dynamic Data { get; private set; }

        /// <summary>
        /// Получает или задает массив всязанных с текущим объектом состояний.
        /// </summary>
        /// <value>
        /// Массив дочерних состояний.
        /// </value>       
        public List<State> LinkedStates { get; set; }

        /// <summary>
        /// Получает или задает название состояния.
        /// </summary>
        /// <value>
        /// Название состояния.
        /// </value>
        public string StateName { get; set; }

        /// <summary>
        /// Получает или задает название сценария.
        /// </summary>
        /// <value>
        /// Название сценария.
        /// </value>
        public string ScriptName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="State" /> class.
        /// </summary>
        /// <param name="scriptName">Name of the script.</param>
        /// <param name="stateName">Name of the state.</param>
        /// <param name="fileReader">The file reader.</param>
        public State(string scriptName, string stateName, FileReader fileReader)
        {
            LinkedStates = new List<State>();

            StateName = stateName;
            ScriptName = scriptName;

            Data = null;

            // Получаем текущий фаил состояния 
            Data = fileReader.GetStateFile(this);
        }

        /// <summary>
        /// Конструктор с отложенной инициализацией Data
        /// </summary>
        public State(string scriptName, string stateName)
        {
            LinkedStates = new List<State>();

            StateName = stateName;
            ScriptName = scriptName;

            Data = null;
        }

        public void UpdateData(FileReader fileReader)
        {
            Data = fileReader.GetStateFile(this);
        }


        /// <summary>
        /// Получает дочерние состояния.
        /// </summary>
        /// <param name="stateParser">Парсер состояний.</param>
        public void GetChildNodes(StateParser stateParser)
        {
            if (Data == null) return;;

            foreach (dynamic node in Data)
            {
                var jsonObjectString = node.ToString();

                if (!stateParser.ObjectIsClear(jsonObjectString)) { continue; }

                LinkedStates.AddRange(stateParser.ParseStates(jsonObjectString, ScriptName)); // Обработка правил
            }

            RemoveDuplicatedLinks();
            HandleEmptyLinks();
        }

        /// <summary>
        /// Обработка пустых дочерних состояний(потенциальные ошибки).
        /// </summary>
        private void HandleEmptyLinks()
        {
            Messages messagesContainer = Messages.GetInstance();

            var states = LinkedStates.FindAll(state => state.Data == null);

            states.ForEach(state =>
            messagesContainer.AddItem(MessageType.Error, $"{this} => {state}")
            );
        }

        /// <summary>
        /// Рекурсивно удаляет дубликаты в массиве дочерних состояний.
        /// </summary>
        private void RemoveDuplicatedLinks()
        {
            for (int i = 0; i < LinkedStates.Count; i++)
            {
                for (int j = 0; j < LinkedStates.Count; j++)
                {
                    if (LinkedStates[i] == this)
                    {
                        LinkedStates.RemoveAt(i);
                        RemoveDuplicatedLinks();
                    }

                    if (i == j) { continue; }

                    if (LinkedStates[j] == LinkedStates[i])
                    {
                        LinkedStates.RemoveAt(i);
                        RemoveDuplicatedLinks();
                    }
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{ScriptName}.{StateName}";
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="state1">The state1.</param>
        /// <param name="state2">The state2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(State state1, State state2)
        {
            return state1.ScriptName.ToLower() == state2.ScriptName.ToLower() && state1.StateName.ToLower() == state2.StateName.ToLower();
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="state1">The state1.</param>
        /// <param name="state2">The state2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(State state1, State state2)
        {
            return state1.ScriptName.ToLower() != state2.ScriptName.ToLower() || state1.StateName.ToLower() != state2.StateName.ToLower();
        }

        protected bool Equals(State other)
        {
            return string.Equals(StateName, other.StateName) && string.Equals(ScriptName, other.ScriptName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((State)obj);
        }

        public override int GetHashCode()
        {
            unchecked { return ((StateName != null ? StateName.GetHashCode() : 0) * 397) ^ (ScriptName != null ? ScriptName.GetHashCode() : 0); }
        }
    }


}
