﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ParserLibrary
{
    public class FileReader
    {
        /// <summary>
        /// Задает или получает корневую директорию
        /// </summary>
        /// <value>
        /// Корневая директория.
        /// </value>
        public string RootDirectory { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileReader"/> class.
        /// </summary>
        /// <param name="rootDirectory">The root directory.</param>
        public FileReader(string rootDirectory)
        {
            RootDirectory = rootDirectory;
        }

        /// <summary>
        /// Загружает фаил.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>Фаил в виде строки.</returns>
        /// <exception cref="AccessViolationException">Не получается загрузить фаил по директории:  + path</exception>
        private string GetFile(string path)
        {
            if (!File.Exists(path))
                return string.Empty;

            Messages messageContainer = Messages.GetInstance();

            var fileName = path.Replace(RootDirectory, "");
            messageContainer.AddItem(MessageType.Open, fileName);

            return File.ReadAllText(path);
        }

        /// <summary>
        /// Получает dynamic объект, содержащий JSON, из строки с файлом.
        /// </summary>
        /// <param name="fileContent">Строка с файлом.</param>
        /// <returns></returns>
        /// <exception cref="System.IO.FileLoadException">Не удается обработать файл: + ex + Environment.NewLine</exception>
        private dynamic GetJson(string fileContent)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject(fileContent);
            }
            catch (Exception ex)
            {
                throw new System.IO.FileLoadException("Не удается обработать файл" + ex + Environment.NewLine);
            }
        }

        /// <summary>
        /// Ищет и возвращает dynamic object с Json, для указанного состояния.
        /// В названии состояния может содержаться название папки.
        /// </summary>
        /// <param name="state">Состояние для поиска.</param>
        /// <returns>Dynamic object с JSON.</returns>
        public dynamic GetStateFile(State state)
        {
            var stateName = state.StateName;
            var pathParts = stateName.Split('-');

            var result = pathParts.Length == 1
                ? GetJson(GetFile($"{RootDirectory}\\{state.ScriptName}\\{state.StateName}.json"))
                : GetJson(GetFile($"{RootDirectory}\\{state.ScriptName}\\{pathParts[0]}\\{stateName}.json"));

            // Есть ли в названии состояния папка ?
            return result;
        }

        /// <summary>
        /// Получает массив начальных состояний скриптов.
        /// </summary>
        /// <returns>Массив начальных состояний скриптов.</returns>
        public State[] GetScripts()
        {
            string[] scriptDirectories = Directory.GetDirectories(RootDirectory);
            State[] result = new State[scriptDirectories.Length];

            for (int i = 0; i < scriptDirectories.Length; i++)
            {
                var pathParts = scriptDirectories[i].Split('\\');
                var scriptName = pathParts[pathParts.Length - 1];
                result[i] = new State(scriptName, "main", this);

            }

            return result;
        }

        public RegexStateSearchRule[] LoadSearchRules(string filePath)
        {
            if (!File.Exists(filePath))
            throw new IOException("Файл с правилами поиска не найден.");

            return (from line in File.ReadAllLines(filePath) where line[0] != '#' select new RegexStateSearchRule(line.Split(' '))).ToArray();

        }

        public string GetFileDirectory  (string filePath)
        {
            FileInfo info = new FileInfo(filePath);

            if (info.Directory != null) return info.Directory.Name;
            else return "";
        }

        private string[] GetFilesRecursively(string sDir)
        {
            var files = new List<string>();

            foreach (string f in Directory.GetFiles(sDir))
            {
                if (new FileInfo(f).Extension != ".json") continue;

                files.Add(f);
            }
            foreach (string d in Directory.GetDirectories(sDir))
            {
                if (new DirectoryInfo(d).Name  == "data")
                    continue;

                files.AddRange(GetFilesRecursively(d));
            }


            return files.ToArray();
        }
            
        public State[] GetAllStates(bool withData)
        {
            List<State> result = new List<State>();
            string[] scriptDirectories = Directory.GetDirectories(RootDirectory);

            foreach (var scriptDir in scriptDirectories)
            {
                foreach (var fileName in GetFilesRecursively(scriptDir))
                {
                    var scriptName = new DirectoryInfo(scriptDir).Name;
                    var stateName = new FileInfo(fileName).Name.Split('.')[0];

                    result.Add((withData)
                        ? new State(scriptName, stateName)
                        : new State(scriptName, stateName, this));
                }

            }

            return result.ToArray();

        }

        public RegexForbiddenStateRule[] LoadForbiddenRules(string filePath)
        {
            if (!File.Exists(filePath))
                throw new IOException("Файл с правилами фильтрации не найден.");

            return (from line in File.ReadAllLines(filePath) where line[0] != '#' select new RegexForbiddenStateRule(line.Split(' '))).ToArray();

        }


    }
}                                                         
