﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserLibrary
{
    public enum MessageType
    {
        Error,
        Open
    }

    /// <summary>
    /// Singleton класс Messages. 
    /// Служит контейнером для сообщений.
    /// </summary>
    public class Messages
    {
        private static Messages _instance;

        private Dictionary<MessageType, List<string>> itemsDictionary;

        public delegate void Method(object sender);
        public event Method Change;

        private Messages()
        {
            itemsDictionary = new Dictionary<MessageType, List<string>>();

            itemsDictionary.Add(MessageType.Error, new List<string>());
            itemsDictionary.Add(MessageType.Open, new List<string>());
        }

        public static Messages GetInstance()
        {
            return _instance ?? (_instance = new Messages());
        }

        public string[] GetMessages(MessageType type)
        {
            return itemsDictionary.ContainsKey(type) ? itemsDictionary[type].ToArray() : null;
        }

        public void AddItem(MessageType type, string content)
        {
            if (Change != null) Change(this);

            if (!itemsDictionary.ContainsKey(type))
            itemsDictionary.Add(type, new List<string>());

            itemsDictionary[type].Add(content);
        }

        public void Clear()
        {
            itemsDictionary = null;
            itemsDictionary = new Dictionary<MessageType, List<string>>();

            itemsDictionary.Add(MessageType.Error, new List<string>());
            itemsDictionary.Add(MessageType.Open, new List<string>());
        }
    }
}
